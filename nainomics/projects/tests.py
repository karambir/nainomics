from django.test import TestCase

from .models import Project

# Create your tests here.
class ModelTest(TestCase):

	def test_project_model(self):
		project = Project.objects.create(name='test')
		self.assertEqual('test', project.name)