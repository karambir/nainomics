from django.db import models

# Create your models here.
class Project(models.Model):
	name = models.CharField(max_length=40)
	description = models.TextField(blank=True)

	def __unicode__(self):
		return self.name